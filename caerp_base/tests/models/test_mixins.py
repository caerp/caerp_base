from unittest.mock import Mock
import pytest
from sqlalchemy import (
    create_engine,
    Column,
    Integer,
)

from caerp_base.models.base import DBBASE
from caerp_base.models.initialize import initialize_sql
from caerp_base.models.mixins import (
    PersistentACLMixin,
)


def test_duplicable_mixin_empty_undefined():
    from caerp_base.models.mixins import DuplicableMixin

    class DummyDuplicable(DuplicableMixin, Mock):
        pass

    obj = DummyDuplicable()

    with pytest.raises(NotImplementedError):
        obj.duplicate()


def test_duplicable_mixin():
    from caerp_base.models.mixins import DuplicableMixin

    class DummyDuplicable(DuplicableMixin, Mock):
        __duplicable_fields__ = [
            'a',
            'b',
        ]

    obj = DummyDuplicable(a=1, b=2, c=3)
    dup = obj.duplicate()
    assert dup.a == obj.a
    assert dup.b == obj.b
    assert dup.c != 3


def test_duplicable_mixin_adhoc_values():
    from caerp_base.models.mixins import DuplicableMixin

    class DummyDuplicable(DuplicableMixin, Mock):
        __duplicable_fields__ = [
            'a',
            'b',
        ]

    obj = DummyDuplicable(a=1, b=2, c=3)
    dup = obj.duplicate(b=12, c=15)
    assert dup.a == obj.a
    assert dup.b == 12
    assert dup.c == 15


def test_persistentaclmixin():
    class ACLMixinSubclass(DBBASE, PersistentACLMixin):
        id = Column(Integer, primary_key=True)

    engine = create_engine('sqlite:///:memory:')
    initialize_sql(engine)

    obj = ACLMixinSubclass()

    # Use __default_acl__, if available
    ACLMixinSubclass.__default_acl__ = [
        ('Allow', 'principal', ('add', 'show'))
    ]

    assert obj.__acl__ == [
        ('Allow', 'principal', ('add', 'show'))
    ]

    # Override __default_acl__
    obj.__acl__ = [
        ('Deny', 'principal', ('add', 'show'))
    ]

    assert obj.__acl__ == [
        ('Deny', 'principal', ('add', 'show'))
    ]

    # delete __acl__, falling back to __default_acl__
    del obj.__acl__

    assert obj.__acl__ == [
        ('Allow', 'principal', ('add', 'show'))
    ]
