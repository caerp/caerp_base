def test_format_mail():
    from caerp_base.mail import format_mail
    assert format_mail("contact@caerp.coop") == "<contact@caerp.coop>"


def test_format_link():
    from caerp_base.mail import format_link
    settings = {'mail.bounce_url': "caerp.coop"}

    assert format_link(settings, "http://test.fr") == \
        "http://caerp.coop/?url=http%3A//test.fr"
